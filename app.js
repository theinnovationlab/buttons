// AUTHENTICATION
var auth = require('./auth');
// EXPRESS SERVER
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var AdmZip = require('adm-zip');

var public_folder_location = "/srv/www/button.theinnovationlab.io/public_html/buttons/public/";

server.listen(1234);

// AUTHENTICATION
app.use(auth);
// view engine setup
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', routes);
//app.use('/users', users);

app.get('/', function(req, res){
  res.render('index.html');
  console.log("received");
  console.log(req.query.q);
 // return res.json({ status:"Expired Number" });
});

var newZip = new AdmZip();

app.get('/download',function(req,res){
// LINK TO THE ZIP FILE
//res.send('<a href="3dfiles.zip">Download</a>');
// SPECIFY LOCATION OF FOLDER TO BE ZIPPED AND ZIP DESTINATION
newZip.addLocalFolder(public_folder_location+'3dfiles/',public_folder_location);
// WRITE ZIP FILE
newZip.writeZip(public_folder_location+'3dfiles.zip');
// DOWNLOAD FILE
setTimeout(function(){ res.redirect("/3dfiles.zip");console.log("timesup"); }, 3000);
//res.download(public_folder_location+'3dfiles.zip');
})

app.post('/stl', function(req, res){
  res.end('Data succesfully send!');
  console.log("RECEIVED STL FROM-> "+req.body.name);
  // WRITE FILE
  fs.writeFile('public/3dfiles/'+req.body.name+'.stl', req.body.stlData, function (err) {
  if (err) throw err;
  console.log('It\'s saved!');
});
 // return res.json({ status:"Expired Number" });

});

var rate = "";

app.get('/heartrate', function(req, res){  
    res.send('Thanks');
    rate = parseInt(req.query.rate,10);
    // PUSH HEARTRATE
    //socket.emit('biometrical', { heartrate: rate });
   
  io.sockets.emit('biometrical', { heartrate: rate });


    // TRACE RESULTS
    console.log(req.query.rate);
  // var strings = ["rad", "bla", "ska"]
  //   var n = Math.floor(Math.random() * strings.length)
  //   res.send(strings[n])
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


io.on('connection', function (socket) {
  //socket.emit('biometrical', { heartrate: 'heartrate value' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
});

app.get('/heartbeat', function(req, res){  
     res.send("received")
  console.log(req.query.q);
  // var strings = ["rad", "bla", "ska"]
  //   var n = Math.floor(Math.random() * strings.length)
  //   res.send(strings[n])
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
