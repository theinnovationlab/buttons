### Button designer ###

* A node webapp that allows you to customise a shirt button with your mobile and saves the 3D model as an STL file on the server. Go to [domain]/download to download a zip with all stl files 
* v0.1

## Still pretty much a BETA ##
* Works only on mobile
* Works only on portrait mode
* Doesn't support older mobiles