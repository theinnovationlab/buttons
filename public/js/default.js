/* ==========================================================
  Base Default JavaScript
  -- Table of Contents --
*/
paper.install(window);

var name,
tool1,
    tool2,
    canvas2D,
    canvas3D,
    circle,
    button3D,
    material,
    resultGeo,
    pathWidth,
    pathHeight,
    pathX,
    pathY,
    rotate,
    holeRadius,
    distance,
    holesX,
    holesY,
    stlData;

var coles = [];

var circleSegments = [];

var vertices = 16;

var scene;

var _window;

var vWidth, vHeight;

Zepto(function($) {


    var displayValue = document.getElementById("received-value");

    /*var socket = io.connect('http://10.1.20.15:3000');
  socket.on('biometrical', function (data) {
    console.log(data.heartrate);
    displayValue.innerHTML = data.heartrate;
  });*/

    canvas2D = document.getElementById("canvas2D");
    canvas3D = document.getElementById("canvas3D");

    // GET WINDOW ELEMENT
    _window = $(window);

    // VIEWPORT SIZE
    vWidth = getViewportSize()[0];
    vHeight = getViewportSize()[1];

    // DRAW UI
    ui();
    // INITIALISE 2D
    init2D();
    // INITIALISE 3D
    init3D();

})

// CREATE WINDOW SIZE METHOD
function getViewportSize() {
    var vWidth = _window.width();
    var vHeight = _window.height();
    return new Array(vWidth, vHeight);
}

function ui() {
    // RESIZE ICONS
    var show2D = $("#show-2d");
    var show3D = $("#show-3d");
    var send   = $('#send-data');

    var iconWidth = {
        width: vWidth * 0.125 + "px"
    };

    show2D.css(iconWidth);
    show3D.css(iconWidth);
    send.css(iconWidth);

    var state2D = true;

    $("#toggle").on("click", function(event) {
        // alert("clicked");
        var display = (state2D) ? [{
                display: "none"
            }, {
                display: "block"
            },
            1, 2
        ] : [{
                display: "block"
            }, {
                display: "none"
            },
            2, 1
        ]
        state2D = (state2D) ? false : true;
        rotate = false;

        show2D.css(display[0]);
        show3D.css(display[1]);
        canvas2D.style.zIndex = display[2];
        canvas3D.style.zIndex = display[3];

        // DRAW 3D MODEL
        if (!state2D) draw3DModels(material);


    });

    $("#send").on("click", function(event) {
        // DRAW 3D
        draw3DModels(material);

	var currentURL = window.location.protocol;

        // SEND DATA TO SERVER
        $.post(currentURL+'/stl', { name: Date.now(),stlData:
generateSTL(resultGeo) }, function(response){
    alert(response);
  // process response
})
    })

}

// DRAWING 2D CANVAS
// CANVAS ELEMENT
function init2D() {

    paper.setup(canvas2D);

    project.currentStyle = {
        fillColor: 'white',
        strokeColor: 'black',
        strokeWidth: '7'
    }

    project.activeLayer.fillColor = new Color(1, 0, 0);

    var whiteTxt = {
        fillColor: "white",
        strokeWidth: "0",
        justification: 'center'
    };

    // PORTRAIT MODE WARNING
    var warning = new Group();
    warning.pivot = warning.bounds.topLeft;
    warning.position = new Point(0, 0);

    var keyline = new Path.Line(new Point(0, 0),
        new Point(vWidth, 0)
    );
    keyline.closed = true;
    keyline.fillColor = "red";
    keyline.strokeColor = 'white'
    keyline.addSegments([new Point(vWidth, vHeight), new Point(0, vHeight)]);
    warning.addChild(keyline);


    var text = new PointText(new Point(0, 0));
    text.style = whiteTxt;

    text.pivot = text.bounds.center;
    text.position = new Point(vWidth * 0.5, vHeight * 0.5);

    // Set the content of the text item:
    text.content = 'THIS APP ONLY WORKS IN PORTRAIT MODE';

    warning.addChild(text);
    warning.opacity = 0;

    function readDeviceOrientation() {
        showOrientationErrorAndResize();
        // THIS APP ONLY WORKS IN PORTRAIT MODE
    }

    window.onorientationchange = readDeviceOrientation

    // DESIGN PORTRAIT WARNING

    function showOrientationErrorAndResize() {
        
        if(window.orientation==90 || window.orientation==-90) alert("The app only works in portrait mode. Please restart the app!");

    }

    var centerX = vWidth * 0.5,
        centerY = vHeight * 0.5,
        radius = vWidth * 0.45,
        angle = 0,
        speed = 0.3,
        numObjects = vertices,
        slice = Math.PI * 2 / numObjects,
        x, y;

    var handlesGroup = new Group();
    handlesGroup.pivot = handlesGroup.bounds.topLeft;
    handlesGroup.position = new Point(0, 0);

    var clickedItem = null;

    var handles = [];
    var handleRadius = vWidth * 0.03;

    for (var i = 0; i < numObjects; i++) {
        drawLine(i);
    };

    // DRAWING THE CIRCLE
    function drawLine(step) {
        angle = step * slice;
        x = centerX + Math.cos(angle) * radius;
        y = centerY + Math.sin(angle) * radius;

        circleSegments.push([x, y]);

        // DRAW HANDLES
        var handle = new Shape.Circle(new Point(x, y), handleRadius)

        handle.name = "  " + step;
        handle.on('mousedown', function() {
            this.fillColor = 'red';
            this.radius = handleRadius * 2;
            clickedItem = this;
        });
        handle.on('mouseup', function() {
            this.fillColor = 'white';
            this.radius = handleRadius;
            clickedItem = null;
        });
        handlesGroup.addChild(handle);
        handles.push(handle);

    }

    circle = new Path({
        segments: circleSegments,
        closed: true
    });

    circle.sendToBack(true)

    // DRAW HOLES
    getBounds();

    holeRadius = pathWidth * 0.05;
    holesX = pathWidth * 0.5 + pathX;
    holesY = pathHeight * 0.5 + pathY;
    distance = pathHeight * 0.1;
    var hole1 = new Path.Circle(new Point(holesX, holesY - distance), holeRadius);
    var hole2 = new Path.Circle(new Point(holesX, holesY + distance), holeRadius);
    hole1.strokeColor = "#999999";
    hole2.strokeColor = "#999999";

    var holesGroup = new Group();
    holesGroup.addChildren([hole1, hole2]);
    var holesBounds = holesGroup.bounds;
    holesX = holesBounds.x;
    holesY = holesBounds.y;

    var tool = new Tool();

    var rect = new Path.Rectangle(new Point(holesX - (holesBounds.width * 0.25), holesY - (holesBounds.height * 0.125)), new Size(holesBounds.width * 1.5, holesBounds.height * 1.25))
    rect.fillColor = null;
    rect.strokeColor = "#999999";
    rect.strokeWidth = 3;
    rect.dashArray = [10, 4];

    tool.onMouseDown = function(event) {

    }

    tool.onMouseMove = function(event) {

        var mPoint = event.point;
        var x = mPoint.x;
        var y = mPoint.y;

        if (clickedItem != null) {

            var itemId = clickedItem.index;

            if (x < holesX - (holesBounds.width * 0.25) || x > holesX + (holesBounds.width * 1.5) || y < holesY - (holesBounds.height * 0.25) || y > holesY + (holesBounds.height * 1.5)) {



                // UPDATING 2D
                circleSegments[itemId] = [x, y];
                clickedItem.position = mPoint;
                circle.segments[itemId].point = mPoint;
            } else {
                // STOP TRACKING
                clickedItem.fillColor = 'white';
                clickedItem.radius = handleRadius;
                clickedItem = null;
            }

        }
    }

    /**/

    //chart.insert(1, [150, 200], [150, 50], [180, 50], [180, 200]);
    view.onFrame = function(event) {
        // On each frame, rotate the path by 3 degrees:

    }
    console.log("finsiehd");
}

function getBounds() {
    // PATH WIDTH
    var b = circle.bounds;
    pathWidth = b.width;
    pathHeight = b.height;
    pathX = b.x;
    pathY = b.y;
    console.log(pathWidth + " " + pathHeight + " " + pathX + " " + pathY);
}

// DRAWING 3D CANVAS   
function init3D() {
    scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);

    var renderer = new THREE.CanvasRenderer({
        canvas: canvas3D
    });

    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
    camera.position.z = 100;
    //camera.position.y = 80;

    material = new THREE.MeshNormalMaterial({
        wireframe: false,
        overdraw: false
    });

    function render() {
        requestAnimationFrame(render);
        renderer.render(scene, camera);
        // IF SHOWING 3D MOVE THE OBJECT
        if (rotate) {
           models[models.length-1].rotation.x += 0.01;
           models[models.length-1].rotation.y += 0.01;
        }
    }
    render();


}

function draw3DModels(material) {

    // GET 2D PATH BOUNDS
    getBounds();

    // REMOVE PREVIOUS OBJECT IF IT EXISTS
    if (undefined != button3D) {

        console.log("DELETE MODEL");
        for (var n = 0; n < models.length; n++) {
            scene.remove(models[n]);
            models[n].geometry.dispose();
            models[n].material.dispose();
        }
    }

    // RESET MODELS
    models = [];

    var buttonPoints = [];
    var noOfVertices = vertices;
    var modelWidth = (pathWidth * 0.1);
    var modelHeight = (pathHeight * 0.1);
    var xFromCenter = (pathX * 0.1);
    var yFromCenter = (pathY * 0.1);
    var xOffset = (modelWidth * 0.5) + xFromCenter;
    var yOffset = (modelHeight * 0.5) + yFromCenter;

    for (var i = 0; i < noOfVertices; i++) {

        var x = ((circleSegments[i][0] * 0.1) - xOffset);
        var y = ((circleSegments[i][1] * 0.1) - yOffset);
        buttonPoints.push(new THREE.Vector2(x, y));
        // console.log(x+" - "+y);

    }
    //console.log(xOffset + " - " + yOffset);
    var buttonShape = new THREE.Shape(buttonPoints);

    var extrusionSettings = {
        amount: 4,
        bevelEnabled: false,
        steps: 1
    };

    var buttonGeometry = new THREE.ExtrudeGeometry(buttonShape, extrusionSettings);


    button3D = new THREE.Mesh(buttonGeometry, material);
    //scene.add(button3D);

    // DRAW CYLINDERS
    var geometry = new THREE.CylinderGeometry(holeRadius * 0.1, holeRadius * 0.1, 20, 10);

    var cylinder1 = new THREE.Mesh(geometry, material);
    var cylinder2 = new THREE.Mesh(geometry, material);

    cylinder1.rotation.x = Math.PI/2;
    cylinder2.rotation.x = Math.PI/2;

    var positionX = ((holesX + holeRadius) * 0.1) - xOffset;
    var positionY1 = ((holesY + holeRadius) * 0.1) - yOffset;
    var positionY2 = ((holesY + distance + (holeRadius * 2)) * 0.1) - yOffset;

    cylinder1.position.set(positionX, positionY1, 0);
    models.push(cylinder1);
    //scene.add( cylinder1 );
    cylinder2.position.set(positionX, positionY2, 0);
    models.push(cylinder2);
    //scene.add( cylinder2 );

    var cylinders = new THREE.Geometry();

    // MERGE CYLINDERS
    THREE.GeometryUtils.merge(cylinders, cylinder1);
    THREE.GeometryUtils.merge(cylinders, cylinder2);

    // ADD MERGED CYLINDERS IN SCENE
    var cylindersMesh = new THREE.Mesh(cylinders, material);
    //scene.add(cylindersMesh);

    // MAKING THE HOLES
    var csgButton = THREE.CSG.toCSG(buttonGeometry);
    var csgCylinders = THREE.CSG.toCSG(cylinders);

    var resultCSG = csgButton.subtract(csgCylinders);

    // BACK CONVERT
    resultGeo = THREE.CSG.fromCSG( resultCSG );

    // HOLED MESH
    var mesh = new THREE.Mesh(resultGeo, material);
    models.push(mesh);

    scene.add( mesh );
    
    // START ROTATING
    rotate = (rotate)? false:true;

  
}

  // CONVERT TO STL
function stringifyVector(vec){
  return ""+vec.x+" "+vec.y+" "+vec.z;
}

function stringifyVertex(vec){
  return "vertex "+stringifyVector(vec)+" \n";
}

function generateSTL(geometry){
  var vertices = geometry.vertices;
  var tris     = geometry.faces;

  stl = "solid pixel";
  for(var i = 0; i<tris.length; i++){
    stl += ("facet normal "+stringifyVector( tris[i].normal )+" \n");
    stl += ("outer loop \n");
    stl += stringifyVertex( vertices[ tris[i].a ]);
    stl += stringifyVertex( vertices[ tris[i].b ]);
    stl += stringifyVertex( vertices[ tris[i].c ]);
    stl += ("endloop \n");
    stl += ("endfacet \n");
  }
  stl += ("endsolid");

  return stl;
}
