function icons(){
    var iconsGroup = new Group();
  // HAPPY HEART
  var happyHeart = new Group();
var hh1 = new Path({segments:[
                            [
                                [29, 36.985],
                                [0.25, 0],
                                [-0.25, 0]
                            ],
                            [
                                [15.717, 23.274],
                                [2.158, 7.976],
                                [-0.922, -3.408]
                            ],
                            [
                                [20.084, 14.28],
                                [-4.1, 1.654],
                                [3.791, -1.529]
                            ],
                            [
                                [29, 16.229],
                                [-1.199, -1.068],
                                [1.199, -1.067]
                            ],
                            [
                                [37.916, 14.28],
                                [-3.791, -1.529],
                                [4.1, 1.654]
                            ],
                            [
                                [42.283, 23.274],
                                [0.922, -3.408],
                                [-2.158, 7.976]
                            ]
                        ],closed: true,strokeColor:"#5DB951"});
happyHeart.addChild(hh1);

// SAD HEART
var sadHeart = new Group();
var sh1 = new Path({segments:[
                            [
                                [29, 36.985],
                                [0.25, 0],
                                [-0.25, 0]
                            ],
                            [
                                [15.717, 23.274],
                                [2.158, 7.976],
                                [-0.922, -3.408]
                            ],
                            [
                                [20.084, 14.28],
                                [-4.1, 1.654],
                                [3.791, -1.529]
                            ],
                            [
                                [29, 16.229],
                                [-1.199, -1.068],
                                [1.199, -1.067]
                            ],
                            [
                                [37.916, 14.28],
                                [-3.791, -1.529],
                                [4.1, 1.654]
                            ],
                            [
                                [42.283, 23.274],
                                [0.922, -3.408],
                                [-2.158, 7.976]
                            ]
                        ],closed: true,strokeColor:"#E54A1D"});
sadHeart.addChild(sh1);
  // HAPPY FACE
  var happyFace = new Group();
var hf1 = new Path({segments:[
                                    [
                                        [41.5, 24.5],
                                        [0, 0],
                                        [0, 6.627]
                                    ],
                                    [
                                        [29.5, 36.5],
                                        [6.627, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [29.5, 36.5],
                                        [0, 0],
                                        [-6.627, 0]
                                    ],
                                    [
                                        [17.5, 24.5],
                                        [0, 6.627],
                                        [0, 0]
                                    ],
                                    [
                                        [17.5, 24.5],
                                        [0, 0],
                                        [0, -6.627]
                                    ],
                                    [
                                        [29.5, 12.5],
                                        [-6.627, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [29.5, 12.5],
                                        [0, 0],
                                        [6.627, 0]
                                    ],
                                    [
                                        [41.5, 24.5],
                                        [0, -6.627],
                                        [0, 1]
                                    ]
                                ],closed: true,strokeColor:"#5DB951"});

  
  var hf2 = new Path({segments:[
                                    [
                                        [30.31, 32.75],
                                        [4.75, 0],
                                        [-4.75, 0]
                                    ],
                                    [24.31, 27.5],
                                    [30.31, 27.5],
                                    [36.31, 27.5]
                                ],closed: true,strokeColor:"#5DB951"});
  
  var hf3 = new Path({segments:[
                                    [23.81, 22.382],
                                    [
                                        [23.81, 22.382],
                                        [0, 0],
                                        [0, -1.381]
                                    ],
                                    [
                                        [26.31, 19.882],
                                        [-1.381, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [26.31, 19.882],
                                        [0, 0],
                                        [1.381, 0]
                                    ],
                                    [
                                        [28.81, 22.382],
                                        [0, -1.381],
                                        [0, 0]
                                    ],
                                    [28.81, 22.382]
                                ],closed: false,strokeColor:"#5DB951"});
  
  var hf4 = new Path({segments:[
                                    [31.81, 22.382],
                                    [
                                        [31.81, 22.382],
                                        [0, 0],
                                        [0, -1.381]
                                    ],
                                    [
                                        [34.31, 19.882],
                                        [-1.381, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [34.31, 19.882],
                                        [0, 0],
                                        [1.381, 0]
                                    ],
                                    [
                                        [36.81, 22.382],
                                        [0, -1.381],
                                        [0, 0]
                                    ],
                                    [36.81, 22.382]
                                ],closed: false,strokeColor:"#5DB951"});
happyFace.addChildren([hf1,hf2,hf3,hf4]);
  // SAD FACE
  var sadFace = new Group();

  var sg1 = new Path({
    segments:[
                                    [23.5, 22.382],
                                    [
                                        [23.5, 22.382],
                                        [0, 0],
                                        [0, -1.381]
                                    ],
                                    [
                                        [26, 19.882],
                                        [-1.381, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [26, 19.882],
                                        [0, 0],
                                        [1.381, 0]
                                    ],
                                    [
                                        [28.5, 22.382],
                                        [0, -1.381],
                                        [0, 0]
                                    ],
                                    [28.5, 22.382]
                                ],
    closed: false,strokeColor:"#E54A1D"
});
  var sg2 = new Path({segments:[
                                    [31.5, 22.382],
                                    [
                                        [31.5, 22.382],
                                        [0, 0],
                                        [0, -1.381]
                                    ],
                                    [
                                        [34, 19.882],
                                        [-1.381, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [34, 19.882],
                                        [0, 0],
                                        [1.381, 0]
                                    ],
                                    [
                                        [36.5, 22.382],
                                        [0, -1.381],
                                        [0, 0]
                                    ],
                                    [36.5, 22.382]
                                ],closed: false,strokeColor:"#E54A1D"});
  
  var sg3 = new CompoundPath({children:[new Path({segments:[[44.873, 22.692],[44.873, 19.272],[47.363, 19.272],[47.363, 22.692],[47.063, 26.53],[45.144, 26.53]],closed: true}),new Path({segments:[[44.931, 28.231],[47.335, 28.231],[47.335, 30.491],[44.931, 30.491]],closed: true})],fillColor:"#E54A1D"});
  
  var sg4 = new Path({segments:[
                                    [
                                        [41.5, 24.5],
                                        [0, 0],
                                        [0, 6.627]
                                    ],
                                    [
                                        [29.5, 36.5],
                                        [6.627, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [29.5, 36.5],
                                        [0, 0],
                                        [-6.627, 0]
                                    ],
                                    [
                                        [17.5, 24.5],
                                        [0, 6.627],
                                        [0, 0]
                                    ],
                                    [
                                        [17.5, 24.5],
                                        [0, 0],
                                        [0, -6.627]
                                    ],
                                    [
                                        [29.5, 12.5],
                                        [-6.627, 0],
                                        [0, 0]
                                    ],
                                    [
                                        [29.5, 12.5],
                                        [0, 0],
                                        [6.627, 0]
                                    ],
                                    [
                                        [41.5, 25],
                                        [0, -6.627],
                                        [0, 0]
                                    ]
                                ],closed: false,strokeColor:"#E54A1D"}); 
  
  var sg5 = new Path({segments:[
                                    [24, 28.5],
                                    [30.002, 28.5],
                                    [36, 28.5]
                                ],closed: false,strokeColor:"#E54A1D"});
  
sadFace.addChildren([sg1,sg2,sg3,sg4,sg5]);

    iconsGroup.addChildren([happyHeart,sadHeart,happyFace,sadFace]);

  return iconsGroup;

  }
